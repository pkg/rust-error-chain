Source: rust-error-chain
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-backtrace-0.3+default-dev (>= 0.3.3-~~) <!nocheck>,
 librust-version-check-0.9+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 kpcyrd <git@rxv.cc>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/error-chain]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/error-chain

Package: librust-error-chain-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-version-check-0.9+default-dev
Recommends:
 librust-error-chain+default-dev (= ${binary:Version})
Suggests:
 librust-error-chain+backtrace-dev (= ${binary:Version})
Provides:
 librust-error-chain+example-generated-dev (= ${binary:Version}),
 librust-error-chain-0-dev (= ${binary:Version}),
 librust-error-chain-0+example-generated-dev (= ${binary:Version}),
 librust-error-chain-0.12-dev (= ${binary:Version}),
 librust-error-chain-0.12+example-generated-dev (= ${binary:Version}),
 librust-error-chain-0.12.4-dev (= ${binary:Version}),
 librust-error-chain-0.12.4+example-generated-dev (= ${binary:Version})
Description: Yet another error boilerplate library - Rust source code
 This package contains the source for the Rust error-chain crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-error-chain+backtrace-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-error-chain-dev (= ${binary:Version}),
 librust-backtrace-0.3+default-dev (>= 0.3.3-~~)
Provides:
 librust-error-chain-0+backtrace-dev (= ${binary:Version}),
 librust-error-chain-0.12+backtrace-dev (= ${binary:Version}),
 librust-error-chain-0.12.4+backtrace-dev (= ${binary:Version})
Description: Yet another error boilerplate library - feature "backtrace"
 This metapackage enables feature "backtrace" for the Rust error-chain crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-error-chain+default-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-error-chain-dev (= ${binary:Version}),
 librust-error-chain+example-generated-dev (= ${binary:Version}),
 librust-backtrace-0.3+default-dev (>= 0.3.3-~~)
Provides:
 librust-error-chain-0+default-dev (= ${binary:Version}),
 librust-error-chain-0.12+default-dev (= ${binary:Version}),
 librust-error-chain-0.12.4+default-dev (= ${binary:Version})
Description: Yet another error boilerplate library - feature "default"
 This metapackage enables feature "default" for the Rust error-chain crate, by
 pulling in any additional dependencies needed by that feature.
